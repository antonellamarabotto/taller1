# -*- coding: utf-8 -*-
"""
Editor de Spyder

Este es un archivo temporal
"""

##Flash 1

#Problema 1
#Funcion que tiende al numero pi dividido dos cuando n tiende al infinito.
def ProblemaDybala(n):
    res=0
    for i in range (1,n+1):
        res=(((-1.0)**(i+1.0))*2.0)/(2.0*i-1.0)+res
    return res
print(ProblemaDybala(1000000))




#Problema 2: Funcion que me devuelve el enesimo numero que cumple dos condiciones


#Condicion 2. Defino una funcion que evalue si un numero es primo o no
def esPrimo(res):
    k=0
    for r in range (1,abs(res)+1):
        if abs(res)%r==0:
            k+=1
    return k==2


#Esta funcion me tiene que devolver el enesimo numero que cumple que es primo y
#se escribe como la ecuacion especificada en es2N1
def Suarez(n):
    i=1#Contador que llega hasta n
    exp=0   
    while i<=n:#Ciclo que frena cuando i es n
        res=2**exp-1#condicion que debe cumplir el nesimo numero
        if esPrimo(res):#segunda condicion que debe cumplir el enesimo. Si no la cumple no es el enesimo numero
            i=i+1
            exp=exp+1
        else:#No encontre el numero que es primo y se escribe con la expresion exponencial. Busco el siguiente numero que se escribe con esa expersion 
            exp=exp+1
    return res
print(Suarez(4))